/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.algo_bfmidterm;

/**
 *
 * @author Thanya
 */
public class A1_11 {
    public static void main(String[] args) {

        int A[] = {3, 5, 4, 3, 5};

        int result = 0;
        for (int i : A) {
            System.out.println(result+" XOR "+i);
            result = result ^ i;
            System.out.println("Result "+result);
        }

        System.out.println(result);

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.algo_bfmidterm;

/**
 *
 * @author Thanya
 */
public class A1_8 {
    public static int[] reverseArr(int[] arr) {

        for (int i = 0; i < arr.length / 2; i++) {
            int temp = arr[arr.length - 1 - i];
            arr[arr.length - 1 - i] = arr[i];
            arr[i] = temp;
        }
        for (int i = 0; i < arr.length; i++) {
            System.out.print(arr[i] + " ");
        }
        return arr;
    }

    public static void main(String[] args) {
        int arr[] = {6, 1, 2, 5, 3, 7, 9};
        reverseArr(arr);
    }

}

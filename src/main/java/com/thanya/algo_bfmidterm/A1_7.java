/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.thanya.algo_bfmidterm;

import java.io.IOException;
import java.util.Scanner;

/**
 *
 * @author Thanya
 */
public class A1_7 {
    static boolean checkPair(int A[], int c) {
        for (int i = 0; i < A.length - 1; i++) {
            for (int j = i + 1; j < A.length; j++) {
                if (A[i] + A[j] == c) {
                    return true;
                }
            }
        }

        return false;
    }

    public static void main(String[] args) {

        int A[] = {2, 8, 2, 4, 2};
        int c = 4;
        if (checkPair(A, c)) {
            System.out.println("Yes");
        } else {
            System.out.println("No");
        }
    }
}

